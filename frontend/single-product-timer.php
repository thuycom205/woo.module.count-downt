<?php
if(!defined('ABSPATH')) {
	exit;
}
if(empty($args['items'])) {
	return;
}

foreach ($args['items'] as $id => $item) {
	if(empty($item['end_date'])) {
		continue;
	}
	
	$style ='';
	if ( isset( $args['active_var'] ) && $args['active_var'] != $id ) {
	
		$style = 'style=" display: none;"';
	
	}
	
	$date = mgc_get_countdown( $item['end_date'] );
	
	?>
	    <div class="mgc-countdown mgc-item-<?php echo $id; ?>" <?php echo $style; ?>>
	        <div class="mgc-header">
	            <?php
	
	            echo apply_filters( 'mgc_timer_title', __( 'Countdown to sale', 'mgc' ), $item['before'] );
	
	            ?>
	        </div>
	        <div class="mgc-timer">
	            <div class="mgc-days">
	                <div class="mgc-amount">
	                    <?php $days = ( ( is_rtl() ) ? strrev( $date['dd'] ) : $date['dd'] ); ?>
	                    <span class="mgc-char-1"><?php echo substr( $days, 0, 1 ); ?></span>
	                    <span class="mgc-char-2"><?php echo substr( $days, 1, 1 ); ?></span>
	                </div>
	                <div class="mgc-label">
	                    <?php _e( 'Days', 'mgc' ) ?>
	                </div>
	            </div>
	            <div class="mgcc-hours">
	                <div class="mgcc-amount">
	                    <?php $hours = ( ( is_rtl() ) ? strrev( $date['hh'] ) : $date['hh'] ); ?>
	                    <span class="mgc-char-1"><?php echo substr( $hours, 0, 1 ); ?></span>
	                    <span class="mgc-char-2"><?php echo substr( $hours, 1, 1 ); ?></span>
	                </div>
	                <div class="mgc-label">
	                    <?php _e( 'Hours', 'mgc' ) ?>
	                </div>
	            </div>
	            <div class="mgc-minutes">
	                <div class="mgc-amount">
	                    <?php $minutes = ( ( is_rtl() ) ? strrev( $date['mm'] ) : $date['mm'] ); ?>
	                    <span class="mgc-char-1"><?php echo substr( $minutes, 0, 1 ); ?></span>
	                    <span class="mgc-char-2"><?php echo substr( $minutes, 1, 1 ); ?></span>
	                </div>
	                <div class="mgc-label">
	                    <?php _e( 'Minutes', 'mgc' ) ?>
	                </div>
	            </div>
	            <div class="mgc-seconds">
	                <div class="mgc-amount">
	                    <?php $seconds = ( ( is_rtl() ) ? strrev( $date['ss'] ) : $date['ss'] ); ?>
	                    <span class="mgc-char-1"><?php echo substr( $seconds, 0, 1 ); ?></span>
	                    <span class="mgc-char-2"><?php echo substr( $seconds, 1, 1 ); ?></span>
	                </div>
	                <div class="mgc-label">
	                    <?php _e( 'Seconds', 'mgc' ) ?>
	                </div>
	            </div>
	        </div>
	    </div>
	    <script type="text/javascript">
	        jQuery(document).ready(function ($) {
	
	            var countdown_div = $('.mgc-item-<?php echo $id;?> .mgc-timer'),
	                countdown_html = countdown_div.clone(),
	                first_char,
	                second_char;
	
	            <?php if (! is_rtl()):?>
	            first_char = ' .mgc-amount .mgc-char-1';
	            second_char = ' .mgc-amount .mgc-char-2';
	            <?php else:?>
	            second_char = ' .mgc-amount .mgc-char-1';
	            first_char = ' .mgc-amount .mgc-char-2';
	            <?php endif;?>
	
	
	            $('.mgc-days' + first_char, countdown_html).text('{d10}');
	            $('.mgc-days' + second_char, countdown_html).text('{d1}');
	
	            $('.mgc-hours' + first_char, countdown_html).text('{h10}');
	            $('.mgc-hours' + second_char, countdown_html).text('{h1}');
	
	            $('.mgc-minutes' + first_char, countdown_html).text('{m10}');
	            $('.mgc-minutes' + second_char, countdown_html).text('{m1}');
	
	            $('.mgc-seconds' + first_char, countdown_html).text('{s10}');
	            $('.mgc-seconds' + second_char, countdown_html).text('{s1}');
	
	            countdown_div.countdown({
	                until : $.countdown.UTCDate(
	                    <?php echo $date['gmt']; ?>,
	                    <?php echo date( 'Y', $date['to'] ); ?>,
	                    <?php echo ( date( 'm', $date['to'] ) - 1 ); ?>,
	                    <?php echo date( 'd', $date['to'] ); ?>),
	                layout: countdown_html.html()
	            });
	
	        });
	    </script>
	
	<?php
	}
		
