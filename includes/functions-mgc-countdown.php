<?php
if(!defined('ABSPATH')) {
	exit;
}
if(!function_exists('mgc_days')) {
	function mgc_days($to) {
		return floor(($to - time()) / 60 / 60 / 24);
	}
}
if(!function_exists('mgc_hours')) {
	function mgc_hours($to) {
		return floor(($to - time()) / 60 / 60);
	}
}
if(!function_exists('mgc_minutes')) {
	function mgc_minutes($to) {
		return floor(($to - time()) / 60);
	}
}
if(!function_exists('mgc_seconds')) {
	function mgc_seconds($to) {
		return $to - time();
	}
}
if(!function_exists('mgc_get_countdown')) {
	function mgc_get_countdown($end_date) {
		$days = mgc_days($end_date);
		$hours = mgc_hours($end_date) - mgc_day($end_date);
		$minutes = mgc_minutes($end_date) - mgc_hours($end_date);
		$seconds = mgc_seconds($end_date) - mgc_minutes($end_date);
	
		return array(
				'gmt' => get_option('gmt_offset'),
				'to'  => $end_date,
				'dd'  => ($days > 10) ? $days : '0' .$days,
				'hh'  => ($hours > 10) ? $hours : '0' .$hours,
				'mm'  => ($minutes > 10) ? $minutes : '0' .$minutes,
				'ss'  => ($seconds > 10) ? $seconds : '0' .$seconds,
		);
	}
	
}