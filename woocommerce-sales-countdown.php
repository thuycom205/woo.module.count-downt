<?php
/*
 * Plugin Name: WooCommerce Sales Countdown
 * Plugin URI: 
 * Description: Add sales countdown function to website
 * Author: Magenest
 * Author URI: http://magenest.com
 * Version: 1.0
 * Text Domain: woocommerce-wishlist
 * Domain Path: /languages/
 */

if (!defined('ABSPATH')) exit;  //Exit if accessed directly

if(!defined('COUNTDOWN_TEXT_DOMAIN'))
	define('COUNTDOWN_TEXT_DOMAIN','countdown');

//Plugin Folder Path
if(!defined('COUNTDOWN_PATH'))
	define('COUNTDOWN_PATH', plugin_dir_path(__FILE__));

//Plugin Folder URL
if(!defined('COUNTDOWN_URL'))
	define ('COUNTDOWN_URL', plugins_url ( 'woocommerce-sales-countdown', 'woocommerce-sales-countdown.php' ) );

//Plugin Root File
if(!defined('COUNTDOWN_FILE'))
	define('COUNTDOWN_FILE', plugin_basename(__FILE__));

class Magenest_Countdown {
	function __construct() {
		include_once( 'includes/functions-mgc-countdown.php' );

		
		if(is_admin()) {
			
			add_action( 'woocommerce_product_options_general_product_data', 
					array($this,'woo_add_countdown_fields'));
			add_action( 'woocommerce_process_product_meta',
					array($this,'woo_add_countdown_fields_save' ),10,2);
			
		}
		else {
			add_action('woocommerce_before_main_content',array($this,'check_show_mgc_product'),5);
			add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
		}
		
	}
	
	function woo_add_countdown_fields() {
		global $woocommerce, $post;
		echo '<div class="options_group">';
		// Checkbox enable
		woocommerce_wp_checkbox(
				array(
					'id' => '_mgc_enabled',
					'wrapper_class' => 'show_if_simple',
					'label' => __('Enable','mgc'),
					'description' => __('Enable Woocommerce Product
							Countdown for this product','mgc')
				));
		//Number totalproduct discount
		woocommerce_wp_text_input(
				array(
					'id' => '_mgc_total_product_discount',
					'label' => __('Total product discount','mgc'),
					'placeholder' => '',
					'desc_tip' => 'true',
					'description' => __('Enter the number of total product discount here','mgc'),
						'type'              => 'number',
						'custom_attributes' => array(
								'step' 	=> 'any',
								'min'	=> '0')
				)
				);
		//Number quantity product sale
		woocommerce_wp_text_input(
				array(
						'id' => '_mgc_quantity_product_sale',
						'label' => __('Quantity product sale','mgc'),
						'placeholder' => '',
						'desc_tip' => 'true',
						'description' => __('Enter the number of product sale here','mgc'),
						'type' => 'number',
						'custom_attributes' => array(
								'step' => 'any',
								'min' => '0',
								'max' => '_mgc_total_product_discount'
						)
				)
				);
		echo '</div>';
	}
	
	function woo_add_countdown_fields_save() {
		
		global $post;
		// Checkbox
		$woocommerce_checkbox = isset($_POST['_mgc_enabled']) ? 'yes' : 'no';
		update_post_meta($post->ID,'_mgc_enabled',$woocommerce_checkbox);
		
		//Total product discount
		$woocommerce_total_product_discount = $_POST['_mgc_total_product_discount'];
		update_post_meta($post->ID,'_mgc_total_product_discount',esc_attr($woocommerce_total_product_discount));
		
		//Quantity product sale
		$woocommerce_quantity_product_sale = $_POST['_mgc_quantity_product_sale'];
		update_post_meta($post->ID,'_mgc_quantity_product_sale',esc_attr($woocommerce_quantity_product_sale));
	}
	
	function check_show_mgc_product() {
		global $post;
		$has_countdown = get_post_meta($post->ID,'_mgc_enabled',true);
		$show_countdown = get_option('mgc_where_show');
		if($has_countdown == 'yes' && $show_countdown != 'loop') {
			$args = apply_filters('mgc_showing_position',
					array('hook' => 'before_single_product',
							'priority' => 5));
			add_action('woocommerce_'.$args['hook'].'_summary', array( $this, 'add_mgc_product' ), $args['priority'] );
		}
	}
	
	function add_mgc_product() {
		$what_show = get_option('mgc_what_show');
		global $post;
		$args = apply_filters('mgc_product_args',array(
				'item' => array(
						$post->ID => array(
								'before' => 'false',
								'end_date' => get_post_meta($post->ID,'_sale_price_dates_to',true)
						))
		),'','');
		if($what_show != 'bar' || $what_show == 'both') {
			include(COUNTDOWN_PATH . '/frontend/single-product-timer.php');
		}
		apply_filters('mgc_product_sales_bar','',$what_show,$args);
		apply_filters( 'ywpc_product_scripts', '', $args );
	}
	
	public function frontend_scripts() {
	
	}
	
}
new Magenest_Countdown();